import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { HashRouter, BrowserRouter, Route } from "react-router-dom";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

import App from "./components/App";
import registerServiceWorker from "./registerServiceWorker";
import store from "./store";

const theme = createMuiTheme({
  palette: {
    common: {
      type: "dark"
    },
    primary: {
      main: "#2E2E37",
      light: "#8D8D91",
      dark: "#54545B",
      contrastText: "#70B6CC"
    },
    secondary: {
      main: "#5C95A7",
      light: "#70B6CC",
      dark: "#3E6470",
      contrastText: "#fff"
    },
    text: {
      primary: "#394875",
      secondaryTextColor: "#394875"
    },
    background: {
      paper: "#2E2E37"
    }
  },
  card: {
    subtitleColor: "70B6CC",
    headlineColor: "#70B6CC"
  },
  typography: {
    fontFamily: "Roboto, sans-serif",
    display1: {
      color: "#70B6CC"
    },
    display2: {
      color: "#394875"
    }
  },
  button: {
    color: "#3E6470"
  },
  zIndex: {
    modal: 1300,
    tooltip: 1500
  }
});

/* global document */

render(
  <Provider store={store}>
    <HashRouter>
      <MuiThemeProvider theme={theme}>
        <Route component={App} />
      </MuiThemeProvider>
    </HashRouter>
  </Provider>,
  document.getElementById("root")
);

registerServiceWorker();

// Hot Module Replacement
if (module.hot) {
  module.hot.accept();
}
